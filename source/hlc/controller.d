module hlc.controller;

import std.conv, std.string, std.stdio, std.json;
import std.experimental.logger;
import vibe.inet.url;
import vibe.inet.webform;

import hlc.entity, hlc.database;
import hlc.http;
import hlc.chronometer;

enum FLAG_NEW = 'n';

public void processRequest(const ref Request req, ref Response res, Database db)
{
	import std.typecons;

	auto url = URL.parse(req.queryString);
	auto elements = url.pathString.split('/');

	switch (elements.count) {
		case 4: // /users/<id>/visits
			if (elements[1] == "users" && elements[3] == "visits" && isNumeric(elements[2])) {
				// show user visits
				userVisits(elements[2].to!uint, url.queryString, res, db);
			} else if (elements[1] == "locations" && elements[3] == "avg" && isNumeric(elements[2])) {
				// /locations/<id>/avg
				locationAvg(elements[2].to!uint, url.queryString, res, db);
			} else {
				res.http400();
			}
			break;

		case 3:
			if ((req.type == RequestType.GET) && isNumeric(elements[2])) {
				// GET /entity/<id>
				switch (elements[1]) {
					case "users":
						get!User(elements[2].to!uint, res, db);
						break;
					case "locations":
						get!Location(elements[2].to!uint, res, db);
						break;
					case "visits":
						get!Visit(elements[2].to!uint, res, db);
						break;
					default:
						res.http400();
						break;
				}

			} else if (req.type == RequestType.POST && isNumeric(elements[2])) {
				// POST /entity/<id>
				try {
					switch (elements[1]) {
						case "users":
							update!User(elements[2].to!uint, req.bodyData, res, db);
							break;
						case "locations":
							update!Location(elements[2].to!uint, req.bodyData, res, db);
							break;
						case "visits":
							update!Visit(elements[2].to!uint, req.bodyData, res, db);
							break;
						default:
							res.http400();
							break;
					}
				} catch (JSONException e) {
					debug writeln(e.msg);
					res.http400();
				}
			} else if (req.type == RequestType.POST && elements[2] == "new") {
				// POST /entity/new
				try {
					switch (elements[1]) {
						case "users":
							add!User(req.bodyData, res, db);
							break;
						case "locations":
							add!Location(req.bodyData, res, db);
							break;
						case "visits":
							add!Visit(req.bodyData, res, db);
							break;
						default:
							res.http400();
							break;
					}
				} catch (JSONException e) {
					debug writeln(e.msg);
					res.http400();
				}
			} else {
				res.http404();
				return;
			}
			break;

		default:
			res.http400();
	}
}

void locationAvg(const uint id, string queryString, ref Response res, Database db)
{
	if (db.hasLocation(id)) {
		cstart!"avg:total";
		cstart!"avg:parse";
		LocationAvgSearchParams params;
		FormFields fields = FormFields.init;

        parseURLEncodedForm(queryString, fields);

		if ("fromDate" in fields) {
			if (fields["fromDate"].isNumeric) {
				params.fromDate = fields["fromDate"].to!long;
			} else {
				res.http400();
				return;
			}
		}
		if ("toDate" in fields) {
			if (fields["toDate"].isNumeric) {
				params.toDate = fields["toDate"].to!long;
			} else {
				res.http400();
				return;
			}
		}
		if ("fromAge" in fields) {
			if (fields["fromAge"].isNumeric) {
				params.toBirthday = db.ageToBirthday(fields["fromAge"].to!long);
			} else {
				res.http400();
				return;
			}
		}
		if ("toAge" in fields) {
			if (fields["toAge"].isNumeric) {
				params.fromBirthday = db.ageToBirthday(fields["toAge"].to!long);
			} else {
				res.http400();
				return;
			}
		}
		if ("gender" in fields) {
			if (fields["gender"] == "m") {
				params.gender = UserGender.M;
			} else if (fields["gender"] == "f") {
				params.gender = UserGender.F;
			} else {
				res.http400();
				return;
			}
		}
		cfinish!"avg:parse";

		double avg = db.getLocationAvg(id, params);

		cstart!"avg:serialize";
		if (avg == 0) {
			res.http200(`{"avg":0}`);
		} else {
			res.http200(`{"avg":` ~ format("%.5f", avg) ~ "}");
		}
		cfinish!"avg:serialize";
		cfinish!"avg:total";
	} else {
		res.http404();
	}
}

void userVisits(const uint id, string queryString, ref Response res, Database db)
{
	import std.algorithm.sorting, hlc.chronometer;

	if (db.hasUser(id)) {
		cstart!"uservisits:total";
		UserVisitSearchParams params;
		FormFields fields = FormFields.init;

		cstart!"uservisits:parse";

		parseURLEncodedForm(queryString, fields);

		if ("fromDate" in fields) {
			if (fields["fromDate"].isNumeric) {
				params.fromDate = fields["fromDate"].to!long;
			} else {
				res.http400();
				return;
			}
		}
		if ("toDate" in fields) {
			if (fields["toDate"].isNumeric) {
				params.toDate = fields["toDate"].to!long;
			} else {
				res.http400();
				return;
			}
		}
		if ("toDistance" in fields) {
			if (fields["toDistance"].isNumeric) {
				params.toDistance = fields["toDistance"].to!long;
			} else {
				res.http400();
				return;
			}
		}
		if ("country" in fields) {
			params.country = fields["country"];
		}

		cfinish!"uservisits:parse";

		cstart!"uservisits:search";
		auto visits = db.getUserVisits(id, params);
		cfinish!"uservisits:search";

		res.reset();
		res.write(`{"visits": [`);

		if (visits.length) {
			auto sortedVisits = sort!"a.visited_at <= b.visited_at"(visits);

			cstart!"uservisits:serialize";
			foreach (visit; sortedVisits) {
				res.write(visit.jsonString);
				res.write(",");
			}
			cfinish!"uservisits:serialize";

			res.decreasePointer(1);
		}

		res.write(`]}`);
		res.send();
		cfinish!"uservisits:total";
	} else {
		res.http404();
	}
}

void get(T)(uint id, ref Response res, Database db)
{
	cstart!(T, "get:total");

	auto entityPtr = db.get!T(id);

	if (entityPtr is null) {
		res.http404();
	} else {
		cstart!(T, "get:serialize");
		res.http200(entityPtr.jsonString);
		cfinish!(T, "get:serialize");
	}

	cfinish!(T, "get:total");
}

void update(T)(uint id, string bodyData, ref Response res, Database db)
{
	cstart!(T, "update:total");

	try {
		cstart!(T, "update:parse");
		JSONValue entity = bodyData.parseJSON();
		cfinish!(T, "update:parse");

		cstart!(T, "update:db");
		static if (is(T == User)) {
			bool isUpdated = db.updateUser(id, entity);
		}
		static if (is(T == Location)) {
			bool isUpdated = db.updateLocation(id, entity);
		}
		static if (is(T == Visit)) {
			bool isUpdated = db.updateVisit(id, entity);
		}

		if (isUpdated) {
			cfinish!(T, "update:db");

			res.http200();
		} else {
			res.http404();
		}

	} catch (JSONException e) {
		res.http400();
	}

	cfinish!(T, "update:total");
}

void add(T)(string jsonString, ref Response res, Database db)
{
	cstart!(T, "add:total");
	cstart!(T, "add:exception");

	try {
		cstart!(T, "add:parse");
		JSONValue json = parseJSON(jsonString);
		cfinish!(T, "add:parse");

		static if (is(T == User)) {
			bool added = db.addUser(json);
		}
		static if (is(T == Location)) {
			bool added = db.addLocation(json);
		}
		static if (is(T == Visit)) {
			bool added = db.addVisit(json);
		}
		if (added == false) {
			trace("Not added");

			res.http400();
		} else {
			trace("Added");

			res.http200();
		}
	} catch (JSONException e) {
		res.http400();
		cfinish!(T, "add:exception");
	}
	cfinish!(T, "add:total");
}

version(unittest)
{
	bool answerBodyIs(Request req, Database db, string expected)
	{
		import std.socket;

		Request     answerRequest;
		Response 	res = Response.init;

		processRequest(req, res, db);
		string data = res.getBuffer().split("\r\n\r\n")[1];

		if (data == expected) {
			res.reset();
			return true;
		} else {
			writeln("=============");
			writefln("Expected: %s", expected);
			writefln("Actual:   %s", data);
			writeln("=============");
			res.reset();
			return false;
		}
	}

	bool answerCodeIs(Request req, Database db, string expected)
	{
		import std.socket;

		Request     answerRequest;
		Response	res;

		processRequest(req, res, db);

		string httpCode = res.getBuffer()
			.split("\r\n\r\n")[0]
			.split("\r\n")[0]
			.split(' ')[1];

		if (httpCode == expected) {
			res.reset();
			return true;
		} else {
			res.reset();
			writeln("=============");
			writefln("Expected: %s", expected);
			writefln("Actual:   %s", httpCode);
			writeln("=============");
			return false;
		}
	}
}

// entity add
unittest
{
	import hlc.chronometer;

	writefln(" +++ Testing %s %s +++", __FILE__, "entity add");
	sharedLog.logLevel = LogLevel.warning;
	chrono.enabled = false;

    Request req = {
        type:          RequestType.POST,
        contentLength: 0,
        bodyData:      ""
    };

	Database db = new Database;

	// test add user with errors
	req.queryString = "/users/new";
	req.bodyData = `{"id":null}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "400"));

	// test add location with errors
	req.queryString = "/locations/new";
	req.bodyData = `{"id":null}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "400"));

	// test add visit with errors
	req.queryString = "/visits/new";
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "400"));


	// test add user with success
	req.queryString = "/users/new";
	req.bodyData = `{"id":1, "email":"email@mail.com", "first_name":"FN1",` ~
		` "last_name":"LN1", "gender":"m", "birth_date":-2}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "200"));

	// test add location with success
	req.queryString = "/locations/new";
	req.bodyData = `{"id":1, "place":"place1", "country":"country1",` ~
		` "city":"city1", "distance":42}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "200"));

	// test add visit with success
	req.queryString = "/visits/new";
	req.contentLength = req.bodyData.length;
	req.bodyData = `{"id":1, "location":1, "user":1,` ~
		` "visited_at":123456, "mark":5}`;
	assert(req.answerCodeIs(db, "200"));


	// test add user with error - already exists
	req.queryString = "/users/new";
	req.bodyData = `{"id":null}`;
	req.contentLength = req.bodyData.length;
	req.bodyData = `{"id":1, "email":"email@mail.com", "first_name":"FN1",` ~
		` "last_name":"LN1", "gender":"m", "birth_date":-2}`;
	assert(req.answerCodeIs(db, "400"));

	// test add location with error - already exists
	req.queryString = "/locations/new";
	req.bodyData = `{"id":null}`;
	req.contentLength = req.bodyData.length;
	req.bodyData = `{"id":1, "place":"place1", "country":"country1",` ~
		` "city":"city1", "distance":42}`;
	assert(req.answerCodeIs(db, "400"));

	// test add visit with error - already exists
	req.queryString = "/visits/new";
	req.bodyData = `{"id":1, "location":1, "user":1,` ~
		` "visited_at":123456, "mark":5}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "400"));

}

// entity update
unittest
{
	import std.socket, hlc.chronometer;
	writefln(" +++ Testing %s %s +++", __FILE__, "entity get");
	sharedLog.logLevel = LogLevel.warning;
	chrono.enabled = false;

    Request req = {
        type:          RequestType.POST,
        contentLength: 0,
        bodyData:      ""
    };

	Database db = new Database;

	JSONValue u = parseJSON(`{
		"id": 1,
		"email":      "e@ma.il",
    	"first_name": "FirstName",
    	"last_name":  "LastName",
    	"gender":     "m",
    	"birth_date": -135
	}`);
	JSONValue l = parseJSON(`{
		"id": 11,
		"place":   "Place1",
		"distance": 11,
		"country": "Country1",
		"city":    "City1"
	}`);
	JSONValue v = parseJSON(`{
		"id": 111,
		"location": 11,
		"user": 1,
		"visited_at": 1200,
		"mark": 4
	}`);

	db.addUser(u);
	db.addLocation(l);
	db.addVisit(v);

	// test update user OK
	req.queryString = "/users/1";
	req.bodyData = `{"email":"email@mail.com", "first_name":"FN1",` ~
		` "last_name":"LN1", "gender":"m", "birth_date":-2}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "200"));

	// test update user 400
	req.queryString = "/users/1";
	req.bodyData = `{"last_name":"LN1", "gender":"m", "birth_date":"bbbb"}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "400"));


	// test update location OK
	req.queryString = "/locations/11";
	req.bodyData = `{"place":"place1", "country":"country1",` ~
		` "city":"city1", "distance":42}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "200"));

	// test update location 400
	req.queryString = "/locations/11";
	req.bodyData = `{"city":"city1", "distance":"dist"}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "400"));


	// test update visit OK
	req.queryString = "/visits/111";
	req.bodyData = `{"location":11, "user":1,` ~
		` "visited_at":123456, "mark":1}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "200"));

	// test update visit 400
	req.queryString = "/visits/111";
	req.bodyData = `{"visited_at":123456, "mark":"m"}`;
	req.contentLength = req.bodyData.length;
	assert(req.answerCodeIs(db, "400"));
}

// users visits
unittest
{
	import std.socket, hlc.chronometer;
	writefln(" +++ Testing %s %s +++", __FILE__, "user visits");
	sharedLog.logLevel = LogLevel.warning;
	chrono.enabled = false;

    Request req = {
        type:          RequestType.GET,
        contentLength: 0,
        bodyData:      ""
    };

	Database db = new Database;

	JSONValue u = [
		"id": 1
	];
	JSONValue u2 = [
		"id": 2
	];
	JSONValue l1 = parseJSON(`{
		"id":       11,
		"place":    "Place1",
		"distance": 11,
		"country":  "Country1"
	}`);
	JSONValue l2 = parseJSON(`{
		"id": 12,
		"place": "Place2",
		"distance": 120,
		"country": "Страна2"
	}`);
	JSONValue v1 = parseJSON(`{
		"id":         111,
		"location":   11,
		"user":       1,
		"visited_at": 1200,
		"mark":       4
	}`);
	JSONValue v2 = parseJSON(`{
		"id":         112,
		"location":   12,
		"user":       1,
		"visited_at": 2888,
		"mark":       5
	}`);

	db.addUser(u);
	db.addUser(u2);
	db.addLocation(l1);
	db.addLocation(l2);
	db.addVisit(v1);
	db.addVisit(v2);

	// test toDistance
	req.queryString = "/users/1/visits?toDistance=50";
	assert(req.answerBodyIs(db,
		`{"visits": [{"mark":4,"visited_at":1200,"place":"Place1"}]}`
	));

	// test fromDate and toDate
	req.queryString = "/users/1/visits?fromDate=1500&toDate=3150";
	assert(req.answerBodyIs(db,
		`{"visits": [{"mark":5,"visited_at":2888,"place":"Place2"}]}`
	));

	// test country
	req.queryString = "/users/1/visits?country=Страна2";
	assert(req.answerBodyIs(db,
		`{"visits": [{"mark":5,"visited_at":2888,"place":"Place2"}]}`
	));

	// test empty result
	req.queryString = "/users/2/visits?country=Страна2";
	assert(req.answerBodyIs(db, `{"visits": []}`));

	// test 404
	req.queryString = "/users/3/visits?country=Страна2";
	assert(req.answerCodeIs(db, "404"));

	// test 400
	req.queryString = "/users/1/visits?toDistance=хехе";
	assert(req.answerCodeIs(db, "400"));

	// test ordering
	req.queryString = "/users/1/visits";
	assert(req.answerBodyIs(db,
		`{"visits": [{"mark":4,"visited_at":1200,"place":"Place1"},{"mark":5,"visited_at":2888,"place":"Place2"}]}`
	));
}

// location avg
unittest
{
	import std.socket, hlc.chronometer;
	writefln(" +++ Testing %s %s +++", __FILE__, "location avg");
	sharedLog.logLevel = LogLevel.warning;
	chrono.enabled = false;

    Request req = {
        type:          RequestType.GET,
        contentLength: 0,
        bodyData:      ""
    };

	Database db = new Database;

	JSONValue u = parseJSON(`{
		"id": 1,
		"gender": "m"
	}`);
	JSONValue u2 = parseJSON(`{
		"id": 2,
		"gender": "m"
	}`);
	JSONValue l1 = parseJSON(`{
		"id": 11,
		"place": "Place1",
		"distance": 11,
		"country": "Country1"
	}`);
	JSONValue l2 = parseJSON(`{
		"id": 12,
		"place": "Place2",
		"distance": 120,
		"country": "Страна2"
	}`);
	JSONValue v1 = parseJSON(`{
		"id": 111,
		"location": 11,
		"user": 1,
		"visited_at": 1200,
		"mark": 4
	}`);
	JSONValue v2 = parseJSON(`{
		"id": 112,
		"location": 12,
		"user": 1,
		"visited_at": 2888,
		"mark": 5
	}`);

	db.addUser(u);
	db.addUser(u2);
	db.addLocation(l1);
	db.addLocation(l2);
	db.addVisit(v1);
	db.addVisit(v2);

	// test avg
	req.queryString = "/locations/11/avg";
	assert(req.answerCodeIs(db, "200"));
	assert(req.answerBodyIs(db,
		`{"avg":4.00000}`
	));

	// test avg by gender
	req.queryString = "/locations/11/avg?gender=f";
	assert(req.answerCodeIs(db, "200"));
	assert(req.answerBodyIs(db,
		`{"avg":0}`
	));
}

// entity get
unittest
{
	import std.socket, hlc.chronometer;
	writefln(" +++ Testing %s %s +++", __FILE__, "entity get");
	sharedLog.logLevel = LogLevel.warning;
	chrono.enabled = false;

    Request req = {
        type:          RequestType.GET,
        contentLength: 0,
        bodyData:      ""
    };

	Database db = new Database;

	JSONValue u = parseJSON(`{
		"id": 1,
		"email":      "e@ma.il",
    	"first_name": "FirstName",
    	"last_name":  "LastName",
    	"gender":     "m",
    	"birth_date": -135
	}`);
	JSONValue l = parseJSON(`{
		"id": 11,
		"place":   "Place1",
		"distance": 11,
		"country": "Country1",
		"city":    "City1"
	}`);
	JSONValue v = parseJSON(`{
		"id": 111,
		"location": 11,
		"user": 1,
		"visited_at": 1200,
		"mark": 4
	}`);

	db.addUser(u);
	db.addLocation(l);
	db.addVisit(v);

	// test get user
	req.queryString = "/users/1";
	assert(req.answerCodeIs(db, "200"));
	assert(req.answerBodyIs(db,
		`{"id":1,"email":"e@ma.il","first_name":"FirstName","last_name":"LastName","gender":"m","birth_date":-135}`
	));

	// test get location
	req.queryString = "/locations/11";
	assert(req.answerCodeIs(db, "200"));
	assert(req.answerBodyIs(db,
		`{"id":11,"place":"Place1","country":"Country1","city":"City1","distance":11}`
	));

	// test get location
	req.queryString = "/visits/111";
	assert(req.answerCodeIs(db, "200"));
	assert(req.answerBodyIs(db,
		`{"id":111,"location":11,"user":1,"visited_at":1200,"mark":4}`
	));

	// test get visit
}
