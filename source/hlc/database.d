module hlc.database;

import std.stdio;
import std.experimental.logger;
import hlc.entity;
import std.json;

__gshared static Database db;

alias UserList     = LHashMap!User;
alias LocationList = LHashMap!Location;
alias VisitList    = LHashMap!Visit;

final class Database
{
    UserList     users;
    LocationList locations;
    VisitList    visits;

    // current timestamp
    uint timestamp;

    public:
        this()
        {
            // Users: 1,000,074, Locations: 761,314, Visits: 10,000,740

            users     = new UserList(1_010_000);
            locations = new LocationList(800_000);
            visits    = new VisitList(10_010_000);
        }

        bool addUser(string jsonString)
        {
            import fast.json;

            auto json = Json!(validateUsed, false)(jsonString);

            User u;
            json.keySwitch!("id", "first_name", "email",  "last_name", "gender", "birth_date")(
               { u.id         = json.read!uint; },
               { u.first_name = json.read!string; },
               { u.email      = json.read!string; },
               { u.last_name  = json.read!string; },
               { u.gender     = json.read!string; },
               { u.birth_date = json.read!long; },
            );

            if (u.id in users) {
                return false;
            }
            users[u.id] = u;
            auto userPtr = &users[u.id];
            userPtr.visitPtrs.reserve(10);
            userPtr.updateGenderVal();
            userPtr.updateJson();

            return true;
        }

        bool addLocation(string jsonString)
        {
            import fast.json;

            auto json = parseJSON(jsonString);

            Location l;
            json.keySwitch!("id", "place", "country", "city", "distance")(
               { l.id         = json.read!uint; },
               { l.place      = json.read!string; },
               { l.country    = json.read!string; },
               { l.city       = json.read!string; },
               { l.distance   = json.read!ubyte; },
            );

            if (l.id in locations) {
                return false;
            }
            locations[l.id] = l;
            auto ptr = &locations[l.id];

            ptr.updateJson();
            ptr.visitPtrs.reserve(15);

            return true;
        }

        bool addVisit(string jsonString)
        {
            import fast.json;

            auto json = parseJSON(jsonString);

            Visit v;
            json.keySwitch!("id", "location", "user", "visited_at", "mark")(
               { v.id         = json.read!uint; },
               { v.location   = json.read!uint; },
               { v.user       = json.read!uint; },
               { v.visited_at = json.read!uint; },
               { v.mark       = json.read!ubyte; },
            );

            if (v.id in visits) {
                return false;
            }
            visits[v.id] = v;
            auto ptr = &visits[v.id];

            ptr.setUser(users[ptr.user]);
            ptr.setLocation(locations[ptr.location]);
            ptr.updateJson();

            users[ptr.user].addVisit(ptr);

            return true;
        }

        bool addUser(ref JSONValue entity)
        {
            uint id = cast(uint)entity["id"].integer;

            if (id in users) {
                return false;
            }

            users[id] = User();
            auto userPtr = &users[id];

            userPtr.id = id;
            auto email = "email" in entity;
            if (email) {
                userPtr.email = email.str;
            }
            auto first_name = "first_name" in entity;
            if (first_name) {
                userPtr.first_name = first_name.str;
            }
            auto last_name = "last_name" in entity;
            if (last_name) {
                userPtr.last_name = last_name.str;
            }
            auto gender = "gender" in entity;
            if (gender) {
                userPtr.gender = gender.str;
            }
            auto birth_date = "birth_date" in entity;
            if (birth_date) {
                userPtr.birth_date = birth_date.integer;
            }

            userPtr.visitPtrs.reserve(10);
            userPtr.updateGenderVal();
            userPtr.updateJson();

            return true;
        }

        bool addLocation(ref JSONValue entity)
        {
            uint id = cast(uint)entity["id"].integer;

            if (id in locations) {
                return false;
            }

            locations[id] = Location();
            auto locationPtr = &locations[id];

            locationPtr.id = id;
            auto place = "place" in entity;
            if (place) {
                locationPtr.place = place.str;
            }
            auto country = "country" in entity;
            if (country) {
                locationPtr.country = country.str;
            }
            auto city = "city" in entity;
            if (city) {
                locationPtr.city = city.str;
            }
            auto distance = "distance" in entity;
            if (distance) {
                locationPtr.distance = cast(ubyte)distance.integer;
            }

            locationPtr.updateJson();
            locationPtr.visitPtrs.reserve(15);

            return true;
        }

        bool addVisit(ref JSONValue entity)
        {
            uint id = cast(uint)entity["id"].integer;

            if (id in visits) {
                return false;
            }

            visits[id] = Visit();
            auto visitPtr = &visits[id];

            visitPtr.id = id;
            auto location = "location" in entity;
            if (location) {
                visitPtr.location = cast(uint)location.integer;
            }
            auto user = "user" in entity;
            if (user) {
                visitPtr.user = cast(uint)user.integer;
            }
            auto visited_at = "visited_at" in entity;
            if (visited_at) {
                visitPtr.visited_at = cast(uint)visited_at.integer;
            }
            auto mark = "mark" in entity;
            if (mark) {
                visitPtr.mark = cast(ubyte)mark.integer;
            }

            visitPtr.setUser(users[visitPtr.user]);
            visitPtr.setLocation(locations[visitPtr.location]);
            visitPtr.updateJson();

            users[visitPtr.user].addVisit(visitPtr);

            return true;
        }

        //nothrow
        T* get(T)(uint id)
        {
            static if (is(T == User)) {
                auto userPtr = id in users;
                if (userPtr) {
                    return userPtr;
                }
            }
            static if (is(T == Location)) {
                auto locationPtr = id in locations;
                if (locationPtr) {
                    return locationPtr;
                }
            }
            static if (is(T == Visit)) {
                auto visitPtr = id in visits;
                if (visitPtr) {
                    return visitPtr;
                }
            }

            return null;
        }

        bool updateUser(uint id, ref JSONValue entity)
        {
            if (id in users) {
                User newUser = users[id];

                auto email = "email" in entity;
                if (email) {
                    newUser.email = email.str;
                }
                auto first_name = "first_name" in entity;
                if (first_name) {
                    newUser.first_name = first_name.str;
                }
                auto last_name = "last_name" in entity;
                if (last_name) {
                    newUser.last_name = last_name.str;
                }
                auto gender = "gender" in entity;
                if (gender) {
                    newUser.gender = gender.str;
                    newUser.updateGenderVal();
                }
                auto birth_date = "birth_date" in entity;
                if (birth_date) {
                    newUser.birth_date = birth_date.integer;
                }

                users[id] = newUser;
                users[id].updateJson();

                return true;
            } else {
                return false;
            }
        }

        bool updateLocation(uint id, ref JSONValue entity)
        {
            auto oldLocationPtr = id in locations;
            if (!oldLocationPtr) {
                return false;
            }

            Location newLocation = *oldLocationPtr;

            auto place = "place" in entity;
            if (place) {
                newLocation.place = place.str;
            }
            auto country = "country" in entity;
            if (country) {
                newLocation.country = country.str;
            }
            auto city = "city" in entity;
            if (city) {
                newLocation.city = city.str;
            }
            auto distance = "distance" in entity;
            if (distance) {
                newLocation.distance = cast(ubyte)distance.integer;
            }

            locations[id] = newLocation;
            locations[id].updateJson();

            return true;
        }

        bool updateVisit(uint id, ref JSONValue entity)
        {
            import std.algorithm;

            auto oldVisitPtr = id in visits;
            if (!oldVisitPtr) {
                return false;
            }

            Visit newVisit = *oldVisitPtr;
            Location* oldLocationPtr;
            uint oldUserId = 0;

            auto location = "location" in entity;
            if (location) {
                newVisit.location = cast(uint)location.integer;
                oldLocationPtr = &locations[oldVisitPtr.location];
            }
            auto user = "user" in entity;
            if (user) {
                newVisit.user = cast(uint)user.integer;
                newVisit.setUser(users[newVisit.user]);
                oldUserId = oldVisitPtr.user;
            }
            auto visited_at = "visited_at" in entity;
            if (visited_at) {
                newVisit.visited_at = cast(uint)visited_at.integer;
            }
            auto mark = "mark" in entity;
            if (mark) {
                newVisit.mark = cast(ubyte)mark.integer;
            }

            visits[id] = newVisit;

            auto visitPtr = &visits[id];

            if (oldLocationPtr) {
                oldLocationPtr.removeVisit(oldVisitPtr);
                visitPtr.setLocation(locations[newVisit.location]);
            }

            if (oldUserId != 0) {
                users[oldUserId].removeVisit(visitPtr);         // removing from old user
            }
            users[newVisit.user].addVisit(visitPtr); // updating user visits index

            visitPtr.updateJson();

            return true;
        }

        /*
            params:
                fromDate - посещения с visited_at > fromDate
                toDate - посещения с visited_at < toDate
                country - название страны, в которой находятся интересующие достопримечательности
                toDistance - возвращать только те места, у которых расстояние от города меньше этого параметра

        */
        UserVisit[] getUserVisits(uint id, ref UserVisitSearchParams params)
        {
            import hlc.chronometer, std.algorithm, std.range;

            UserVisit[] userVisits;
            userVisits.reserve(users[id].visitPtrs.length);

            //auto filterDelegate = &params.match;
            //sort!("a.visited_at < b.visited_at")( users[id].visitPtrs[].filter!(filterDelegate) );

            foreach (visit; users[id].visitPtrs) {
                version(StdLoggerDisableLogging)
                {} else
                {
                    cstart!"uservisits:search:item";
                }
                if (params.match(visit)) {
                    UserVisit uv = {
                        mark:       visit.mark,
                        visited_at: visit.visited_at,
                        place:      visit.locationPtr.place
                    };
                    userVisits ~= uv;
                }  else {
                    version(HlcDetailedRequestLog) {
                        tracef("  Checking visit %d: visited_at=%s,  mark=%d,  locationDistance=%s,  place=%s, " ~
                            "country=%s NO",
                            visit.id,
                            visit.location,
                            visit.visited_at,
                            visit.mark,
                            visit.locationPtr.distance,
                            visit.locationPtr.place,
                            visit.locationPtr.country
                        );
                    }
                }

                version(StdLoggerDisableLogging)
                {} else
                {
                    cfinish!"uservisits:search:item";
                }

                version(HlcDetailedRequestLog) {
                    tracef("  Checking visit %d: visited_at=%s,  mark=%d,  locationDistance=%s,  place=%s, " ~
                        "country=%s MATCHED. Count=%d",
                        visit.id,
                        visit.location,
                        visit.visited_at,
                        visit.mark,
                        visit.locationPtr.distance,
                        visit.locationPtr.country,
                        userVisits.length
                    );
                }
            }

            version(HlcDetailedRequestLog) {
                tracef("  Found visits: %d", userVisits.length);
            }

            return userVisits;
        }

        /*
            params:
                fromDate - учитывать оценки только с visited_at > fromDate
                toDate - учитывать оценки только с visited_at < toDate
                fromAge - учитывать только путешественников, у которых возраст (считается от текущего timestamp) больше этого параметра
                toAge - как предыдущее, но наоборот
                gender - учитывать оценки только мужчин или женщин
         */
        double getLocationAvg(uint id, ref LocationAvgSearchParams params)
        {
            import std.conv;

            uint count = 0;
            double markSum = 0.0;

            version(HlcDetailedRequestLog)
            {
                tracef("Searching by criteria location %s:", id);

                foreach (visitPtr; locations[id].visitPtrs) {
                    writef("  Checking visit %d: ", visitPtr.id, visitPtr.location);

                    if (params.match(visitPtr)) {
                        count++;
                        markSum += visitPtr.mark;

                        writefln(" count=%d, sum=%f, avg=%.1f", count, markSum, markSum / count);
                    } else {
                        writeln(" NO");
                    }
                }
            } else {
                foreach (visitPtr; locations[id].visitPtrs) {
                    if (params.match(visitPtr)) {
                        count++;
                        markSum += visitPtr.mark;
                    }
                }
            }

            if (count > 0) {
                return markSum / count;
            } else {
                return 0.0;
            }
        }

        bool hasLocation(uint id)
        {
            return (id in locations) !is null;
        }

        bool hasUser(uint id)
        {
            return (id in users) !is null;
        }

        nothrow @nogc @safe
        void setTimestamp(in uint timestamp)
        {
            this.timestamp = timestamp;
        }

        /// convert age to unix timestamp in long format
        long ageToBirthday(long age)
        {
            import std.datetime;

            DateTime dateTime = cast(DateTime)SysTime(timestamp.unixTimeToStdTime());
            dateTime.add!"years"(-age);

            long timestampAge = (cast(SysTime)dateTime).toUnixTime();
            version(HlcDetailedRequestLog)
            {
                writefln("  Age %d to timestamp: %d", age, timestampAge);

                return timestampAge;
            } else {
                return timestampAge;
            }
        }

        unittest
        {
            import std.datetime;

            auto db = new Database;
            uint age = 51;
            db.setTimestamp(1503016202);

            assert(db.ageToBirthday(age) == -106442998);
        }
}

final class LHashMap(Type)
{
    uint length;
    Type[] data;

    this(uint length)
    {
        this.length = length;
        data = new Type[length];
    }

    void opIndexAssign(Type value, in uint key)
	{
        import std.conv;

		assert(key < length, "Index " ~ key.to!string ~ " is bigger than " ~ Type.stringof ~
            " hashmap size(" ~ length.to!string ~ ").");
		data[key] = value;
	}

	ref inout(Type) opIndex(uint key) inout
    {
        import std.conv;

		return data[key];
	}

	inout(Type)* opBinaryRight(string op)(uint key)
	inout if (op == "in")
    {
        import std.conv;

        if (key >= length) {
            return null;
        }
        assert(key < length, "Index " ~ key.to!string ~ " bigger than " ~ Type.stringof  ~
            " hashmap size(" ~ length.to!string ~ ").");
        if (data[key].id == 0) {
            return null;
        }

        return &data[key];
	}
}

// user visit seach matchers

alias UserVisitMatcher = bool delegate(Visit* visit);

struct UserVisitSearchParams
{
    long   fromDate;
    long   toDate;
    long   toDistance;
    string country;

    ubyte paramCount = 0;

    bool match(Visit* visit)
    {
        return
            ((fromDate == 0) || (fromDate < visit.visited_at)) &&
            ((toDate == 0) || (toDate > visit.visited_at)) &&
            ((toDistance == 0) || (toDistance > visit.locationPtr.distance)) &&
            ((country.length == 0) || (country == visit.locationPtr.country))
            ;
    }
}

// locations avg matcher

struct LocationAvgSearchParams
{
    long       fromDate;
    long       toDate;
    long       toBirthday;   //fromAge;
    long       fromBirthday; //toAge;
    UserGender gender = UserGender.U;

    bool match(Visit* visitPtr)
    {
        return
            ((fromDate == 0)      || (fromDate < visitPtr.visited_at)) &&
            ((toDate == 0)        || (toDate > visitPtr.visited_at)) &&
            ((toBirthday == 0)    || (toBirthday   > visitPtr.userPtr.birth_date)) &&
            ((fromBirthday == 0)  || (fromBirthday < visitPtr.userPtr.birth_date)) &&
            ((gender == UserGender.U) || (gender == visitPtr.userPtr.genderVal))
            ;
    }
}
