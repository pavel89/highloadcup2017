module hlc.http;

import std.conv, std.stdio, std.socket;
import std.experimental.logger;
import vibe.utils.string;
import libasync;
import hlc.driver.libasync;

enum RequestType
{
    GET, POST
}

struct Request
{
    RequestType    type;
    string         queryString;
    size_t         contentLength;
    string         bodyData;

    bool keepAlive = true;

    bool parse(ref ubyte[] buffer, size_t bufsize)
    {
        import std.array : split;

        auto headAndBody = split( cast(string)buffer[0..bufsize], "\r\n\r\n");
        auto queryAndHeaders = split(headAndBody[0], "\r\n");
        readRequestLine(queryAndHeaders[0]);
        if (type == RequestType.POST) {
            readPostHeaders(queryAndHeaders);
            if (contentLength == 0) {
                return false;
            }
        } else {
            readGetHeaders(queryAndHeaders);
        }

        if (contentLength > 0) {
            bodyData = (cast(string)(headAndBody[1]));
        }

        return true;
    }

    /// Read first request line
    void readRequestLine(string linebuf) @safe
    {
        auto parts = linebuf.split(' ');

        if (parts[0] == "GET") {
            type = RequestType.GET;
        } else if (parts[0] == "POST") {
            type = RequestType.POST;
        } else {
            writefln("Unknown HTTP method: %s", parts[0]);
        }

        queryString = parts[1].to!string;
    }

    /// Read all headers and parse content length if present
    void readGetHeaders(ref string[] lines)
    {
        ubyte i = 1;
        do {
            if (lines[i].startsWith("Content-Length: ")) {
                tracef("Getting content length from header");

                auto contentLengthHeader = lines[i].split(' ');

                contentLength = contentLengthHeader[1].to!size_t();
            }
            if (lines[i].startsWith("Connection: ")) {
                keepAlive = lines[i][12] == 'k';
            }
            i++;
        } while (i < lines.count);
    }

    void readPostHeaders(ref string[] lines)
    {
        ubyte i = 1;
        do {
            if (lines[i].startsWith("Content-Length: ")) {
                tracef("Getting content length from header");

                auto contentLengthHeader = lines[i].split(' ');

                contentLength = contentLengthHeader[1].to!size_t();
            }
            if (lines[i].startsWith("Connection: ")) {
                version(HlcDetailedRequestLog)
				{
					tracef("Detected Connection header. Keep-alive is %s", lines[i][12] == 'k');
				}
                keepAlive = lines[i][12] == 'k';
            }
            i++;
        } while (i < lines.count);
    }

    unittest
    {
        ubyte[] buf = new ubyte[4096];
        Request req;
        string content=
            "POST /users/1/visits?fromDistance=61 HTTP/1.1\r\n" ~
            "Host: 10.0.3.151:8080\r\n" ~
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n" ~
            "Accept-Language: en-US,en;q=0.5\r\n" ~
            "Accept-Encoding: gzip, deflate\r\n" ~
            "Connection: keep-alive\r\n" ~
            "Content-Length: 58\r\n" ~
            "Cache-Control: max-age=0\r\n" ~
            "\r\n" ~
            `{"some json data here":"Какое-то значение"}`
            ;

        buf[0..content.length] = cast(ubyte[])content;

        req.parse(buf, content.length);

        assert(req.type == RequestType.POST);
        assert(req.queryString == "/users/1/visits?fromDistance=61");
        assert(req.contentLength == 58);
        assert(req.bodyData == `{"some json data here":"Какое-то значение"}`);
    }
}

struct Response
{
    Connection  conn;
    bool        keepAlive = true;

	ubyte[8192] buffer = void;
	uint        pointer = 0;
	uint        contentLength = 0;
	uint        contentLengthPointer = 0;

    void decreasePointer(uint count)
    {
        pointer -= count;
        contentLength -= count;
    }

    @trusted
	void write(ref string str)
	{
		version(HlcDetailedRequestLog)
		{
			tracef("Writing %d bytes to buffer from ref string.", str.length);
		}
		buffer[pointer..pointer+str.length] = cast(ubyte[])str;
		pointer += str.length;
		contentLength += str.length;
	}

    /// for content only
	void write(string str)
	{
		version(HlcDetailedRequestLog)
		{
			tracef("Writing %d bytes to buffer from string.", str.length);
		}
		buffer[pointer..pointer+str.length] = cast(ubyte[])str;
		pointer += str.length;
		contentLength += str.length;
	}

	void send()
	{
		version(HlcDetailedRequestLog)
		{
			tracef("Sending %d bytes to connection from buffer.", pointer);
		}

		string contentLengthStr = contentLength.to!string;
		buffer[contentLengthPointer..contentLengthPointer+contentLengthStr.length] = cast(ubyte[])contentLengthStr;

        version(unittest) {
        } else {
		    conn.send(buffer[0..pointer]);
            reset();
        }
		version(HlcDetailedRequestLog) {
			tracef("Sent buffer %s", cast(string)buffer[0..pointer]);
		}
	}

	void reset()
	{
		immutable static string headers = "HTTP/1.1 200 OK\r\n" ~
            "Content-Type: application/json; charset=UTF-8\r\n" ~
            "Content-Length:      \r\n\r\n";

		pointer = headers.length;
		contentLength = 0;
		contentLengthPointer = 0;

		buffer[0..pointer] = cast(ubyte[])headers;
		contentLengthPointer = pointer - 9;
	}

    void http200(in string content)
    {
        string outbuf = "HTTP/1.1 200 OK\r\n" ~
            "Connection: keep-alive\r\n" ~
            "Content-Length: " ~ content.length.to!string ~ "\r\n" ~
            "\r\n" ~
            content;

        version(HlcDetailedRequestLog)
        {
            tracef("Sending data: %s", outbuf);
        }

        version(unittest) {
            write(outbuf);
        } else {
            conn.send(cast(ubyte[])outbuf);
        }
    }

    void http200()
    {
        immutable string answer = "HTTP/1.1 200 OK\r\n" ~
            "Connection: keep-alive\r\n" ~
            "Content-Length: 2\r\n\r\n{}";

        version(HlcDetailedRequestLog)
        {
            tracef("Sending data: %s", answer);
        }

        version(unittest) {
            write(answer);
        } else {
            conn.send(cast(ubyte[])answer);
        }
    }

    void http404()
    {
        immutable string answer = "HTTP/1.1 404 Not Found\r\nConnection: keep-alive\r\nContent-Length: 2\r\n\r\n{}";

        version(HlcDetailedRequestLog)
        {
            tracef("Sending data: %s", answer);
        }

        version(unittest) {
            write(answer);
        } else {
            conn.send(cast(ubyte[])answer);
        }
    }

    void http400()
    {
        version(unittest) {
            write("HTTP/1.1 400 Bad Request\r\nConnection: keep-alive\r\nContent-Length: 2\r\n\r\n{}");
        } else {
            conn.send(cast(ubyte[])
                "HTTP/1.1 400 Bad Request\r\nConnection: keep-alive\r\nContent-Length: 2\r\n\r\n{}"
            );
        }
    }

    string getBuffer()
    {
        return cast(string)buffer[0..pointer];
    }
}
