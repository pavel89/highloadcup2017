module hlc.chronometer;

import core.time;

version(StdLoggerDisableLogging)
{} else
{
	__gshared AvgChronometer chrono;
}

void cstart(alias T : string)()
{
    version(StdLoggerDisableLogging)
    {} else
    {
        chrono.startSMA(T);
    }
}

void cfinish(alias T : string)()
{
    version(StdLoggerDisableLogging)
    {} else
    {
        chrono.tickSMA(T);
    }
}

struct AvgChronometer
{
    AvgChronometerCounter[string] counters;
    bool enabled = true;

    void declareCounter(string label)
    {
        if (enabled) {
            counters[label] = AvgChronometerCounter.init;
        }
    }

    void startSMA(string label)
    {
        if (enabled) {
            counters[label].start();
        }
    }

    ulong tickSMA(string label)
    {
        if (enabled) {
            counters[label].tick();

            return counters[label].count;
        } else {
            return 0;
        }
    }

    Duration getAVG(string label)
    {
        if (enabled) {
            return counters[label].avg;
        } else {
            return Duration.init;
        }
    }

    void resetSMA(string label)
    {
        counters[label] = AvgChronometerCounter.init;
    }
}

struct AvgChronometerCounter
{
    MonoTime lastBefore;
    MonoTime after;
    Duration current;

    ulong    count = 0;
    Duration avg;

    void start()
    {
        lastBefore = MonoTime.currTime;
    }

    void tick()
    {
        after = MonoTime.currTime;
        current = after - lastBefore;

        /// simple moving average counter
        count++;
        avg = avg + (current - avg) / count;

        lastBefore = after;
    }
}
