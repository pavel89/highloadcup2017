module hlc.loader;

import std.file, std.stdio, std.zip, std.string, std.conv;
import std.experimental.logger;
import std.json;
import hlc.database, hlc.entity, hlc.utils;

final class Loader
{
    public:
        uint userCounter = 0;
        uint locationCounter = 0;
        uint visitCounter = 0;

    protected:
        Database db;

    public:
        this(Database db)
        {
            this.db = db;
        }

        void loadFromDir(string dirname, string[] entityTypes)
        {
            import std.file, std.path, std.algorithm;

            writefln("Reading %s from %s", entityTypes.join(", "), dirname);

            DirEntry[][string] entries = [
                "users":     [],
                "locations": [],
                "visits":    []
            ];

            foreach (DirEntry e; dirEntries(dirname, SpanMode.breadth)) {
                string name = e.name.baseName.split("_")[0];
                if (entityTypes.canFind(name)) {
                    entries[name] ~= e;
                }
            }

            infof("Parsing %s ...", "users");
            foreach (DirEntry e; entries["users"]) {
                load!User(e.readText(), e.name, "users");
            }
            infof("Parsing %s ...", "locations");
            foreach (DirEntry e; entries["locations"]) {
                load!Location(e.readText(), e.name, "locations");
            }
            infof("Parsing %s ...", "visits");
            foreach (DirEntry e; entries["visits"]) {
                load!Visit(e.readText(), e.name, "visits");
            }
        }

        void loadFromZip(string filepath, string[] entityTypes)
        {
            import std.algorithm.searching;

            writefln("Reading %s from %s", entityTypes.join(", "), filepath);

            auto zip = new ZipArchive(filepath.read());
            ArchiveMember[][string] ams;

            // sorting files by entity name
            foreach (ArchiveMember am; zip.directory) {
                string name = am.name.split("_")[0];
                if (entityTypes.canFind(name)) {
                    ams[name] ~= am;
                }
            }

            if ("users" in ams) {
                info("Parsing users ...");
                foreach (ArchiveMember am; ams["users"]) {
                    zip.expand(am);
                    load!User(cast(string)am.expandedData, am.name, "users");
                }
                utils.gcCollect();
            }
            if ("locations" in ams) {
                info("Parsing locations ...");
                foreach (ArchiveMember am; ams["locations"]) {
                    zip.expand(am);
                    load!Location(cast(string)am.expandedData, am.name, "locations");
                }
                utils.gcCollect();
            }
            if ("visits" in ams) {
                info("Parsing visits ...");

                ubyte gcThreshold = 0;
                foreach (ArchiveMember am; ams["visits"]) {
                    zip.expand(am);
                    load!Visit(cast(string)am.expandedData, am.name, "visits");

                    gcThreshold++;
                    if (gcThreshold % 10 == 0) {
                        utils.gcCollect();
                    }
                }
            }
        }

        uint loadTimestamp(string filename)
        {
            import std.file, std.datetime, std.conv;

            uint nowTime = cast(uint)Clock.currTime.toUnixTime;
            if (filename.exists()) {
                warningf("%s found. Loading timestamp.", filename);
                auto lines = readText(filename).split("\n");
                nowTime = lines[0].to!uint;
                warningf("Timestamp is %s.", nowTime);

                utils.setFire(lines[1]);
            } else {
                warningf("%s not found. Setting timestamp to %d.", filename, nowTime);
            }

            return nowTime;
        }

    protected:
        void load(T)(string data, string filename, string jsonKey)
        {
            JSONValue json = parseJSON(data);

            tracef("%8s items found in %s", json[jsonKey].array.length, filename);

            foreach (JSONValue value; json[jsonKey].array) {
                static if (is(T == User)) {
                    db.addUser(value);
                    userCounter++;
                }
                static if (is(T == Location)) {
                    db.addLocation(value);
                    locationCounter++;
                }
                static if (is(T == Visit)) {
                    db.addVisit(value);
                    incVisitCounter();
                }
            }
        }

        void incVisitCounter()
        {
            visitCounter++;

            if (visitCounter % 500_000 == 0) {
                infof("    %,d", visitCounter);
                utils.gcCollect(false);
            }
        }
}
