module hlc.driver.accept;
/+
import core.thread;
import std.socket, std.experimental.logger, std.parallelism;
import hlc.database, hlc.chronometer, hlc.utils;

alias Connection = Socket;

void runServer(Database db, ushort port)
{
	import core.atomic, core.memory;

    auto listenAddress = new InternetAddress("0.0.0.0", port);
    auto serverSocket = new TcpSocket;
    uint backlog = 1024;
    Socket clientSocket;

	taskPool.priority = Thread.PRIORITY_MAX;
    infof("Task pool size: %d", taskPool.size);

	serverSocket.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, 1);
	serverSocket.bind(listenAddress);
    serverSocket.listen(backlog);

	infof("Listening on " ~ listenAddress.toString());

	while (true) {
		clientSocket = serverSocket.accept();
		cstart!"socket:puttask";

		auto task = scopedTask!handleSocket(clientSocket, db);
		taskPool.put(&task);
		cfinish!"socket:puttask";
    }
}

void handleSocket(Socket clientSocket, Database db)
{
	import hlc.http, hlc.controller;
	import core.thread, core.atomic;

	ubyte[] buffer = new ubyte[4096];
	bool isClosed = true; //concurrentConnections >= 2;

	cstart!"socket:read";
	clientSocket.setOption(SocketOptionLevel.TCP, SocketOption.TCP_NODELAY, true);

	do {
		size_t received = clientSocket.receive(buffer);
		cfinish!"socket:read";

		if (received == size_t.max) {
			version(StdLoggerDisableLogging)
			{} else
			{
				warning("size_t error. Closing connection");
			}
			isClosed = true;
		} else if (received > 0) {

			version(HlcDetailedRequestLog)
			{
				tracef("Received %d bytes from socket", received);
				trace(cast(string)buffer[0..received]);
			} else {

			}

			cstart!"req:total";

			Request req;
            Response res;
            res.conn = clientSocket;

			if (req.parse(buffer, received)) {
                res.keepAlive = req.keepAlive;

				version(StdLoggerDisableLogging) {
					processRequest(req, clientSocket, db);
				} else {
					try {
						processRequest(req, res, db);
					} catch (Exception e) {
						criticalf(e.info.toString());
						criticalf("Req string: %s", req.queryString);
						criticalf("Body data: %s", req.bodyData);
					}
				}
			} else {
				version(HlcDetailedRequestLog)
				{
					tracef("Error during request parse");
				} else {

				}
				res.http400();
			}

			if (res.keepAlive == false) {
				version(HlcDetailedRequestLog)
				{
					tracef("Keep-alive disabled. closing connection.");
				}
				isClosed = true;
			}

			cfinish!"req:total";

			utils.incRequestCount();
		} else if (received == Socket.ERROR) {
			version(HlcDetailedRequestLog)
			{
				trace("Connection error on clientSocket.");
			}
			isClosed = true;
		} else if (received == 0) {
			version(HlcDetailedRequestLog)
			{
				trace("Client connection closed.");
			}
			isClosed = true;
		}
	} while (isClosed == false);

	trace("Closing connection");

	clientSocket.close();
} +/
