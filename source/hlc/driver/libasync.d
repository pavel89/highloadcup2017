module hlc.driver.libasync;

import libasync;
import std.stdio, std.experimental.logger;
import hlc.database, hlc.utils, hlc.chronometer, hlc.http;

alias Connection = AsyncTCPConnection;

EventLoop g_evl;
TCPListener g_listener;
bool g_closed;

void runServer(Database db, ushort port = 8080, string address = "0.0.0.0")
{
	import libasync.posix : seconds;
	import std.functional;

	g_evl = new EventLoop;
	g_listener = new TCPListener(address, port);

	version(HlcCustomGC) {
		AsyncTimer g_timerMulti = new AsyncTimer(g_evl);
		auto timerDelegate = toDelegate(&timerTick);
		g_timerMulti.periodic().duration(1.seconds).run(timerDelegate);
    }

	while(!g_closed) {
		g_evl.loop();
	}
	destroyAsyncThreads();
}

final class TCPListener {
	AsyncTCPListener listener;

	this(string host, size_t port)
	{
		listener = new AsyncTCPListener(g_evl);
		listener.noDelay = true;
		listener.ip(host, port);

		if (listener.run(&handler)) {
			writeln("Listening to ", listener.local.toString());
		}

	}

	void delegate(TCPEvent) handler(AsyncTCPConnection conn) {
		auto tcpConn = new TCPConnection(conn);
		return &tcpConn.handler;
	}
}

final class TCPConnection
{
	AsyncTCPConnection conn;
	ubyte[] buffer;
	uint 	buflen;

	Request req;

	this(AsyncTCPConnection conn)
	{
		this.conn = conn;
		this.conn.noDelay = true;
		buffer = new ubyte[4096];
	}

	void onConnect()
	{
		onRead();
	}

	// Note: All buffers must be empty when returning from TCPEvent.READ
	void onRead()
	{
		while (true) {
			buflen = conn.recv(buffer);

			version(HlcDetailedRequestLog)
			{
				tracef("Received %d bytes from socket", buflen);
				if (buflen > 0) {
					trace(cast(string)buffer[0..buflen]);
				}
			}

			if (buflen < buffer.length) {
				break;
			}
		}

		onWrite();
	}

	void onWrite()
	{
		import hlc.controller;

		if (buflen == 0) {
			return;
		}
		Response res;

		cstart!"req:total";

		res.conn = conn;

		if (req.parse(buffer, buflen)) {
			res.keepAlive = req.keepAlive;

			version(StdLoggerDisableLogging) {
				processRequest(req, res, db);
			} else {
				try {
					processRequest(req, res, db);
				} catch (Exception e) {
					criticalf(e.info.toString());
					criticalf("Req string: %s", req.queryString);
					criticalf("Body data: %s", req.bodyData);
				}
			}
		} else {
			version(HlcDetailedRequestLog)
			{
				tracef("Error during request parse");
			} else {

			}
			res.keepAlive = false;
			res.http400();
		}

		if (res.keepAlive == false) {
			version(HlcDetailedRequestLog) {
				tracef("Keep-alive disabled. closing connection.");
			}
			conn.kill(true);
		}
		cfinish!"req:total";

		buflen = 0;
		utils.incRequestCount();
	}

	void onClose()
	{
		//warningf("Connection closed");
	}

	void handler(TCPEvent ev)
	{
		final switch (ev) {
			case TCPEvent.CONNECT:
				onConnect();
				break;
			case TCPEvent.READ:
				onRead();
				break;
			case TCPEvent.WRITE:
				onWrite();
				break;
			case TCPEvent.CLOSE:
				onClose();
				break;
			case TCPEvent.ERROR:
				//assert(false, "Error during TCP Event");
		}
		return;
	}

}
