module hlc.entity;

import std.container, std.conv;
import hlc.chronometer;

enum UserGender
{
    U = -1, // undefined

    F = 0,
    M = 1
}

struct User
{
    uint   id;
    string email;
    string first_name;
    string last_name;
    string gender;
    long   birth_date;

    UserGender genderVal;

    Array!(Visit*) visitPtrs;

    string jsonString;

    //nothrow
    void addVisit(Visit* visitPtr)
    {
        foreach (Visit* vPtr; visitPtrs) {
            if (vPtr == visitPtr) {
                return;
            }
        }

        visitPtrs ~= visitPtr;
    }

    void removeVisit(Visit* visitPtr)
    {
        import std.algorithm, std.range;

        visitPtrs.linearRemove( find(visitPtrs[], visitPtr).takeOne() );
    }

    void updateGenderVal()
    {
        genderVal = gender == "m" ? UserGender.M : UserGender.F;
    }

    void updateJson()
    {
        import std.conv;

        jsonString = `{` ~
            `"id":`          ~ id.to!string ~
            `,"email":"`      ~ email ~
            `","first_name":"` ~ first_name ~
            `","last_name":"`  ~ last_name ~
            `","gender":"`     ~ gender ~
            `","birth_date":` ~ birth_date.to!string ~
        `}`;
    }
}

struct Location
{
    uint   id;
    ubyte  distance;
    string place;
    string country;
    string city;

    Array!(Visit*) visitPtrs;

    string jsonString;

    void addVisit(Visit* visitPtr)
    {
        foreach (Visit* vPtr; visitPtrs) {
            if (vPtr == visitPtr) {
                return;
            }
        }

        visitPtrs ~= visitPtr;
    }

    void removeVisit(Visit* visitPtr)
    {
        import std.algorithm, std.range;

        visitPtrs.linearRemove( find(visitPtrs[], visitPtr).takeOne() );
    }

    void updateJson()
    {
        import std.conv;

        jsonString = `{` ~
            `"id":`         ~ id.to!string ~
            `,"place":"`    ~ place ~
            `","country":"` ~ country ~
            `","city":"`    ~ city ~
            `","distance":` ~ distance.to!string ~
        `}`;
    }
}

struct Visit
{
    uint  id;
    uint  location;
    uint  user;
    uint  visited_at;
    ubyte mark;

    User* userPtr;

    Location* locationPtr;

    @property
    string jsonString()
    {
        return `{` ~
            `"id":`         ~ id.to!string ~
            `,"location":`  ~ location.to!string ~
            `,"user":`      ~ user.to!string ~
            `,"visited_at":`~ visited_at.to!string ~
            `,"mark":`      ~ mark.to!string ~
        `}`;
    }

    string jsonUserVisitString;

    void setUser(ref User u)
    {
        userPtr = &u;
    }

    void setLocation(ref Location l)
    {
        locationPtr = &l;
        locationPtr.addVisit(&this);
    }

    void updateJson()
    {
        import std.conv;

        jsonUserVisitString = `{` ~
            `"mark":` ~ mark.to!string ~
            `"visited_at":` ~ visited_at.to!string ~
            `"place":` ~ (*locationPtr).place ~
        `}`;
    }
}

//deprecated
struct UserVisit
{
    ubyte  mark;
    uint   visited_at;
    string place;

    @property
    string jsonString()
    {
        return `{` ~
            `"mark":` ~ mark.to!string ~
            `,"visited_at":` ~ visited_at.to!string ~
            `,"place":"` ~ place ~
        `"}`;
    }
}

bool visitLess(in Visit* a, in Visit* b)
{
    return a.visited_at < b.visited_at;
}

void cstart(E, alias T : string)()
{
    version(StdLoggerDisableLogging)
    {} else
    {
        static if (is(E == User)) {
            chrono.startSMA("users:" ~ T);
        }
        static if (is(E == Location)) {
            chrono.startSMA("locations:" ~ T);
        }
        static if (is(E == Visit)) {
            chrono.startSMA("visits:" ~ T);
        }
    }
}

void cfinish(E, alias T : string)()
{
    version(StdLoggerDisableLogging)
    {} else
    {
        static if (is(E == User)) {
            chrono.tickSMA("users:" ~ T);
        }
        static if (is(E == Location)) {
            chrono.tickSMA("locations:" ~ T);
        }
        static if (is(E == Visit)) {
            chrono.tickSMA("visits:" ~ T);
        }
    }
}


