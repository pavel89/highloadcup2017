module hlc.utils;

import core.memory, core.time;
import std.experimental.logger;
import hlc.chronometer;

shared Utils utils;

enum Fire
{
    UNKNOWN,
    TEST,
    RATING
}


void timerTick()
{
    import std.stdio;

    if (utils.gcTimerEnabled) {
        auto sinceLastRequest = MonoTime.currTime - utils.lastRequest;

        if (sinceLastRequest.total!"msecs" > 600) {
            version(StdLoggerDisableLogging)
            {} else
            {
                import std.algorithm.sorting, core.memory, core.time;

                infof("Requests processed: %d.", utils.requestCounter);
                utils.gcInfo();
                info("-------------------");

                string[long] countersList;

                foreach (key, counter; chrono.counters) {
                    countersList[ counter.avg.total!"usecs" ] = key;
                    chrono.resetSMA(key);
                }
                foreach (key; countersList.keys.sort!"a > b") {
                    infof("  %23s: %s μs", countersList[key], key);
                }
            }

            infof("%d requests. Collecting garbage...", utils.requestCounter);
            utils.gcCollect(true);
            utils.gcTimerEnabled = false;
        }
    }
}

struct Utils
{
    Fire fire = Fire.UNKNOWN;
    MonoTime lastRequest;
    bool  gcTimerEnabled = false;
    ulong requestCounter = 0;

    shared
    void gcInfo(string prefix = "")
    {
        infof("%s GC free: %,d KB, used: %,d KB", prefix, GC.stats.freeSize / 1024, GC.stats.usedSize / 1024);
    }

    shared
    void gcReserve()
    {
        import std.conv;
        size_t gcUsed = GC.stats.freeSize + GC.stats.usedSize;
        size_t maxSz = 1024*1024*1024*4;


        size_t sz = 1024*1024*512;

        infof("Reserving %d KB of memory", sz / 1024);
        GC.reserve(sz);
    }

    shared
    void gcDisable()
    {
        GC.disable();
    }

    shared
    void gcCollect(bool printStat = false)
    {
        if (printStat) {
            gcInfo("Before");
        }
        GC.collect();
        if (printStat) {
            gcInfo("After");
        }
    }

    shared
    void incRequestCount()
    {
	    import core.atomic;

        version(StdLoggerDisableLogging)
        {} else
        {
            atomicOp!"+="(requestCounter, 1);
        }

        version(HlcCustomGC) {
            lastRequest = MonoTime.currTime;
            gcTimerEnabled = true;
        }
    }

    shared
    void setFire(string firechar)
    {
        switch (firechar) {
            case "0":
                infof("Test fire");
                fire = Fire.TEST;
                break;
            case "1":
                infof("Rating fire");
                fire = Fire.RATING;
                break;
            default:
                infof("Unknown fire: %s", firechar);
                break;
        }
    }
}
