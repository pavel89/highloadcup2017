import std.stdio, std.getopt, std.process, std.socket, std.typecons;
import std.file;
import std.experimental.logger;
import hlc.database, hlc.loader, hlc.chronometer, hlc.utils;
import hlc.driver.libasync : runServer;

immutable string versionString = "0.1.2 libasync+lowmem+ldc2";
byte   verbosity = -1; // log verbosity level
bool   ver;

ushort port = 8080;
string dataFile = "/tmp/zipdata";
//string dataFile = "data.zip";
immutable string optionsFile = "/tmp/data/options.txt";
string address = "0.0.0.0";

int main(string[] args)
{
	if (processHelpInformation(args)) {
        return 0;
    }

	version (StdLoggerDisableLogging)
	{} else
	{
		switch (verbosity) {
			case 0:
				sharedLog.logLevel = LogLevel.critical;
				break;
			case 1:
				sharedLog.logLevel = LogLevel.warning;
				break;
			case 2:
				sharedLog.logLevel = LogLevel.info;
				break;
			case 3:
				sharedLog.logLevel = LogLevel.trace;
				break;

			case -1:
				version(HlcDetailedRequestLog) {
					sharedLog.logLevel = LogLevel.trace;
				} else {
					sharedLog.logLevel = LogLevel.info;
				}
				break;

			default:
				sharedLog.logLevel = LogLevel.info;
				writeln("Unknown verbosity level: %d", verbosity);
				return 1;
		}
	}

    readVersion("version.txt");

	version(StdLoggerDisableLogging)
	{} else
	{
        import core.cpuid;
        infof("Running version %s", versionString);
        infof("Running with PID %s", thisProcessID());

		chrono.declareCounter("socket:puttask");
		chrono.declareCounter("req:total");

		chrono.declareCounter("uservisits:total");
		chrono.declareCounter("uservisits:parse");
		chrono.declareCounter("uservisits:search");
		chrono.declareCounter("uservisits:search:item");
		chrono.declareCounter("uservisits:serialize");

		chrono.declareCounter("avg:total");
		chrono.declareCounter("avg:parse");
		chrono.declareCounter("avg:serialize");

		chrono.declareCounter("users:get:total");
		chrono.declareCounter("locations:get:total");
		chrono.declareCounter("visits:get:total");

		chrono.declareCounter("users:get:serialize");
		chrono.declareCounter("locations:get:serialize");
		chrono.declareCounter("visits:get:serialize");

		chrono.declareCounter("users:update:total");
		chrono.declareCounter("locations:update:total");
		chrono.declareCounter("visits:update:total");

		chrono.declareCounter("users:update:parse");
		chrono.declareCounter("locations:update:parse");
		chrono.declareCounter("visits:update:parse");

		chrono.declareCounter("users:update:db");
		chrono.declareCounter("locations:update:db");
		chrono.declareCounter("visits:update:db");

		chrono.declareCounter("users:add:total");
		chrono.declareCounter("locations:add:total");
		chrono.declareCounter("visits:add:total");

		chrono.declareCounter("users:add:parse");
		chrono.declareCounter("locations:add:parse");
		chrono.declareCounter("visits:add:parse");

		chrono.declareCounter("users:add:exception");
		chrono.declareCounter("locations:add:exception");
		chrono.declareCounter("visits:add:exception");
	}

	db = new Database();
	loadData(db);

	version(HlcCustomGC) {
		utils.gcCollect();
		//utils.gcReserve();
		utils.gcDisable();
	}
	runServer(db, port);

	return 0;
}

void loadData(Database db)
{
    import std.file, std.path, std.format;

    if (dataFile.exists()) {
        auto loader = new Loader(db);
		version(StdLoggerDisableLogging)
		{} else
		{
			AvgChronometer loadChrono;
			loadChrono.declareCounter("loadzip");
			loadChrono.startSMA("loadzip");
		}

		if (dataFile.isDir) {
			loader.loadFromDir(dataFile, ["users", "locations", "visits"]);
		} else {
			loader.loadFromZip(dataFile, ["users", "locations", "visits"]);
		}

		version(StdLoggerDisableLogging)
		{} else
		{
			loadChrono.tickSMA("loadzip");
			infof("Loaded data in %s", loadChrono.getAVG("loadzip"));
			infof("Users: %,d, Locations: %,d, Visits: %,d",
				loader.userCounter,
				loader.locationCounter,
				loader.visitCounter
			);
		}

        db.setTimestamp( loader.loadTimestamp(optionsFile) );
		utils.gcCollect(true);
    } else {
        writefln("Data file %s not found, loading skipped.", dataFile);
    }
}

void readVersion(string filename)
{
    if (filename.exists()) {
        infof("Commit: %s", filename.readText());
	}
}

bool processHelpInformation(string[] args)
{
    import std.conv, std.file, std.path;
    const string helpString = "Highloadcup challenge server.\n\n" ~
        "Usage: " ~ thisExePath().relativePath() ~ " [OPTIONS]";

    auto helpInformation = getopt(args,
        std.getopt.config.caseSensitive,
		"dataFile","[/path/to/data] path to data.zip file (./data.zip by default).", &dataFile,
        "port",    "[1..65535] Port number to listen to (8080 by default).", &port,
        "address", "[IP address] Address to bind to (0.0.0.0 by default).",   &address,

        "version|V",  "Print version and exit.",     &ver,
        "verbose|v",  "[0..3] Use verbose output level. Available levels: " ~
            "0(default, least verbose), 1, 2, 3(most verbose).",         &verbosity
    );

    if (ver) {
        writefln("Highloadcup version %s", versionString);

        return true;
    }

    if (helpInformation.helpWanted) {
        defaultGetoptPrinter(helpString, helpInformation.options);
        return true;
    }

    return false;
}
