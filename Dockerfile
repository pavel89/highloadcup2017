FROM ubuntu:16.04

# Workdir
WORKDIR /root

# Install software
RUN apt-get update && apt-get -y upgrade && \
    apt-get install -y curl wget unzip xz-utils libssl-dev libevent-dev libev-dev libssl-dev
RUN curl -fsS https://dlang.org/install.sh | bash -s ldc
RUN curl -fsS https://dlang.org/install.sh | bash -s dmd
RUN wget http://master.dl.sourceforge.net/project/d-apt/files/d-apt.list -O /etc/apt/sources.list.d/d-apt.list
RUN apt-get update && apt-get -y --allow-unauthenticated install --reinstall d-apt-keyring
RUN apt-get update && apt-get install -y dub

COPY ./dub.sdl /root

RUN dub upgrade

# Copy sources
COPY . /root

# Build server
RUN dub build --build=release --compiler=/root/dlang/ldc-1.3.0/bin/ldc2 && rm -rf .dub
#RUN dub build --build=release-nobounds --config=prod-metrics
#RUN dub build --build=release-nobounds && rm -rf .dub/

# Expose port 80
EXPOSE 80

# Run server
CMD unzip -q /tmp/data/data.zip -d /tmp/zipdata && ./highloadcup --port 80 --dataFile "/tmp/zipdata"
#CMD ./highloadcup --port 80 --dataFile "/tmp/data/data.zip"
