#!/bin/sh
git rev-parse HEAD > version.txt
docker build -t hlc . && \
docker tag hlc stor.highloadcup.ru/travels/dragonfly_winner && \
docker push stor.highloadcup.ru/travels/dragonfly_winner
